const bodyParser = require('body-parser')
const cors = require('cors')
const express = require('express')
const { MongoClient }  = require('mongodb')
const app = express()
var corsOptions = {
  origin:"http://localhost:4200"
}
app.use(cors(corsOptions))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);
app.get('/api/league/:league/season/:season', function (req, res) {
  const league_season_collection_name = req.params.league + '_' + req.params.season;
  getCollectionByName(league_season_collection_name)
  .then((data) => {
    res.json(data);
  })
  .catch(console.error)
  .finally(() => client.close());
});
// Get Documents from Models Collection
app.get('/api/model/:model', function (req, res) {
  const model_collection_name = req.params.model + '_Model';
  getCollectionByName(model_collection_name)
  .then((data) => {
    res.json(data);
  })
  .catch(console.error)
  .finally(() => client.close());
});
// Get Documents from Data Collection
app.get('/api/data/:data', function (req, res) {
  const data_collection_name = req.params.data + '_Data';
  getCollectionByName(data_collection_name)
  .then((data) => {
    res.json(data);
  })
  .catch(console.error)
  .finally(() => client.close());
});


const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

async function getCollectionByName(collection_name) {
  await client.connect();
  console.log('Connected successfully to server');
  const db = client.db('optaDB');
  const collection = db.collection(collection_name);
  const findResult = await collection.find({}).toArray();
  return findResult;
}
