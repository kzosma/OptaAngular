import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSidenavModule } from '@angular/material/sidenav';
import {MatSelectModule} from '@angular/material/select';
import { MatFormFieldModule }  from '@angular/material/form-field';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatTabsModule,
    MatSidenavModule,
    MatSelectModule,
    MatFormFieldModule
  ],
  exports: [
    MatTabsModule,
    MatSidenavModule,
    MatSelectModule,
    MatFormFieldModule
  ]
})
export class MaterialModule { }
