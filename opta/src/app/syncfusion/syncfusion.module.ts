import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartModule,
  LineSeriesService,
  CategoryService,
  LegendService,
  DataLabelService,
  PolarSeriesService,
 TooltipService,
 ZoomService} from '@syncfusion/ej2-angular-charts';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { ProfitabilityComponent } from './profitability/profitability.component';
import { XgchainComponent } from './xgchain/xgchain.component';
@NgModule({
  declarations: [
    ProfitabilityComponent,
    XgchainComponent
  ],
  imports: [
    CommonModule,
    ChartModule,
    ButtonModule
  ],
  providers: [
    LineSeriesService,
    CategoryService,
    LegendService,
    DataLabelService,
    PolarSeriesService,
    ZoomService,
    TooltipService],
    exports: [ProfitabilityComponent,
      XgchainComponent],
    schemas:[CUSTOM_ELEMENTS_SCHEMA],
})
export class SyncfusionModule { }
