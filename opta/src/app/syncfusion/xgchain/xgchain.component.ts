import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { OptaApiService } from 'src/app/services/opta-api.service';

@Component({
  selector: 'app-xgchain',
  templateUrl: './xgchain.component.html',
  styleUrls: ['./xgchain.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class XgchainComponent implements OnInit {

  @Input() league1 = '';
  @Input() season1 = '';
  @Input() team1 = '';
  @Input() league2 = '';
  @Input() season2 = '';
  @Input() team2 = '';
  @Input() title = '';
  @Input() yAxisLabel = '';
  @Input() chartId = '';
  @Input() chartType = '';

  apiService: OptaApiService;
  idChart = '';
  typeChart =  '';
  xAxis: Object;
  yAxis: Object = {};
  chartTitle: String = '';
  public markerSettings: Object;
  public legend: Object;
  public tooltipSettings: Object;
  public zoomSettings: Object;
  stats_players_data_array: any;
  datasource_team1: any[] = [];
  datasource_team2: any[] = [];
  constructor(private optaAPI: OptaApiService ) {
    this.apiService = optaAPI;
    this.xAxis = {
      title: 'Index',
      valueType: 'Category'
    }
    this.legend = {
      visible: true
    }
    this.markerSettings = {
      visible: true,
      dataLabel: {
        visible: true
      }
    }
    this.tooltipSettings = {
      enable: true
    }
    this.zoomSettings = {
      enableMouseWheelZooming: false,
      enablePinchZooming: false,
      enableSelectionZooming: false
    }
   }
  ngOnInit(): void {
    this.chartTitle = this.title;
    this.idChart = this.chartId;
    this.typeChart = this.chartType;
    this.yAxis = {
      title: this.yAxisLabel
    }
  }
  analyser() {
    this.apiService.getDocumentsFromData('Stats_Players')
    .subscribe((data) => {
      const stats_players_data_league1 = data
      .filter((l:any) => l.league === this.league1);
      const stats_players_data_array1 = stats_players_data_league1[0].seasons.filter((s: any)=> s.year === this.season1);
      let series1 = stats_players_data_array1[0].teams;
      if(this.team1 && this.team1.length > 0) {
        series1 = series1.filter((line: any) => line.name === this.team1);
      }
      if(this.team1 !== this.team2 ) {
        series1[0].histo.forEach((value: any,index: number) => {
          value.date = index + 1 ;
        });
      }
      this.datasource_team1 = series1[0].histo;
      console.log(this.datasource_team1);
      const stats_players_data_league2 = data
      .filter((l:any) => l.league === this.league2);
      const stats_players_data_array2 = stats_players_data_league2[0].seasons.filter((s: any)=> s.year === this.season2);
      let series2 = stats_players_data_array2[0].teams;
      if(this.team2 && this.team2.length > 0) {
        series2 = series2.filter((line: any) => line.name === this.team2);
      }
      if(this.team1 !== this.team2 ) {
        series2[0].histo.forEach((value: any,index: number) => {
          value.date = index + 1 ;
        });
      }
      this.datasource_team2 = series2[0].histo;
    });
  }
}
