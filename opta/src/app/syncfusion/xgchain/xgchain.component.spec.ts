import { ComponentFixture, TestBed } from '@angular/core/testing';

import { XgchainComponent } from './xgchain.component';

describe('XgchainComponent', () => {
  let component: XgchainComponent;
  let fixture: ComponentFixture<XgchainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ XgchainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(XgchainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
