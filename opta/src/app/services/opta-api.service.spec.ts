import { TestBed } from '@angular/core/testing';

import { OptaApiService } from './opta-api.service';

describe('OptaApiService', () => {
  let service: OptaApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OptaApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
