import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OptaApiService {

 private http: HttpClient;
 constructor(private httpClient: HttpClient) {
  this.http = httpClient;
  console.log('Opta Service > Constructor', this.http);
 }

 // Get Documents from Models Collection
 getDocumentsFromModel(model: string): Observable<any> {
  return this.http.get('/api/model/'+model);
 }
  // Get Documents from Models Collection
  getDocumentsFromData(dataName: string): Observable<any> {
    console.log('appel : ', dataName);
    return this.http.get('/api/data/'+dataName);
   }
}
