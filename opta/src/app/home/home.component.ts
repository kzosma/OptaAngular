import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { OptaApiService } from '../services/opta-api.service';
import {FormControl} from '@angular/forms';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  // SERVICES
  optaApiService: OptaApiService;
  // Data
  teamsData: any;
  // Leagues
  leagues1: any[] = [];
  league1Selected  = '';
  leagues2: any[] = [];
  league2Selected  = '';

  // Seasons
  seasons1: any[] = [];
  season1Selected = '';
  seasons2: any[] = [];
  season2Selected = '';

  // Teams
  teams1: any[] = [];
  team1Selected = '';
  teams2: any[] = [];
  team2Selected = '';

  // Players
  players_teams1_List: any[] = [];
  players_teams2_List: any[] = [];
  players_teams1_FC = new FormControl();
  players_teams2_FC = new FormControl();

  // Data
  statDataPlayers: any;

  constructor(private optaApi: OptaApiService) {
    this.optaApiService = optaApi;
   }
  ngOnInit(): void {
    this.optaApiService.getDocumentsFromData('Stats_Players')
    .subscribe((data) => {
      this.statDataPlayers = data;
      this.resetData();
    });
  }
  resetData() {
    this.teamsData = undefined;
    this.leagues1 = [];
    this.seasons1 = [];
    this.teams1 = [];
    this.season1Selected = '';
    this.team1Selected = '';
    this.league1Selected = '';
    this.leagues2 = [];
    this.seasons2 = [];
    this.teams2 = [];
    this.season2Selected = '';
    this.team2Selected = '';
    this.league2Selected = '';
    this.players_teams1_List = [];

    this.optaApiService.getDocumentsFromData('Teams')
    .subscribe((data) => {
      this.teamsData = data;
      this.setLeagues(this.teamsData);
    });

  }
  selectTabChange = (matTabEvent: MatTabChangeEvent) => {
    if(matTabEvent.index === 0) {
      this.loadProfitabilityData();
    }
  }
  // Chargement des données Profitabilité des équipes top 5
  loadProfitabilityData() {
    this.resetData();
  }
  setLeagues(data: []) {
    this.leagues1 = [];
    this.leagues2 = [];
    data.forEach((line: any) => {
      this.leagues1.push(line.league);
      this.leagues2.push(line.league);
    });
  }
  setSeasons1(data: []) {
    this.seasons1 = [];
    this.season1Selected = '';
    data.forEach((line: any) => {
      this.seasons1.push(line.year);
    });
  }
  setSeasons2(data: []) {
    this.seasons2 = [];
    this.season2Selected = '';
    data.forEach((line: any) => {
      this.seasons2.push(line.year);
    });
  }
  // Changement de league
  onLeague1Change(league: any) {
    this.seasons1 = [];
    this.season1Selected = '';
    this.teams1 = [];
    this.team1Selected = '';
    this.players_teams1_List = [];
    const seasonsTemp = this.teamsData.filter(
      (obj: { league: any; }) => obj.league === league.value);
    this.setSeasons1(seasonsTemp[0].seasons);

  }
  onLeague2Change(league: any) {
    this.seasons2 = [];
    this.season2Selected = '';
    this.teams2 = [];
    this.team2Selected = '';
    const seasonsTemp = this.teamsData.filter(
      (obj: { league: any; }) => obj.league === league.value);
    this.setSeasons2(seasonsTemp[0].seasons);

  }
  // Changement de saison
  onSeason1Change(season: any) {
    this.teams1 = [];
    this.team1Selected = '';
    this.players_teams1_List = [];
    const teamsFilter_league = this.teamsData.filter((line: any) => {
      return line.league === this.league1Selected
    });
    const teamsFilter = teamsFilter_league[0].seasons.filter((line: any) => {
      return line.year === this.season1Selected
    });
    this.teams1 = teamsFilter[0].teams;
  }
  onSeason2Change(season: any) {
    this.teams2 = [];
    this.team2Selected = '';
    const teamsFilter_league = this.teamsData.filter((line: any) => {
      return line.league === this.league2Selected
    });
    const teamsFilter = teamsFilter_league[0].seasons.filter((line: any) => {
      return line.year === this.season2Selected
    });
    this.teams2 = teamsFilter[0].teams;
  }
  onTeam1Selected(team: any) {
    const stats_players_data_league1 = this.statDataPlayers
      .filter((l:any) => l.league === this.league1Selected);
    const stats_players_data_season1 = stats_players_data_league1[0].seasons.filter((s: any)=> s.year === this.season1Selected);
    const stats_players_data_players1 = stats_players_data_season1[0].teams.filter((s: any)=> s.name === this.team1Selected);
    this.players_teams1_List = stats_players_data_players1[0].lineups;
  }
}
